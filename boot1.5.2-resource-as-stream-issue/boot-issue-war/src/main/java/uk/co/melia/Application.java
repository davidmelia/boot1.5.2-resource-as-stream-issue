package uk.co.melia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.Ordered;

@SpringBootApplication
public class Application {

   public static void main(String[] args) {
      SpringApplication.run(Application.class, args);
   }

   @Bean
   @Lazy(false)
   public ServletRegistrationBean someServlet() {
      ServletRegistrationBean bean = new ServletRegistrationBean();
      bean.setLoadOnStartup(1);
      bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
      bean.setServlet(new SomeServlet());
      return bean;
   }

}
