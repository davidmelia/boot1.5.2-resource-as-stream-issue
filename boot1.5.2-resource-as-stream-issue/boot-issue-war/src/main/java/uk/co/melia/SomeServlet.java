package uk.co.melia;

import java.io.InputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class SomeServlet extends HttpServlet {

   @Override
   public void init(ServletConfig config) throws ServletException {
      InputStream is = config.getServletContext().getResourceAsStream("/scripts/lib/dave.js");
      if (is == null) {
         throw new ServletException("Cannot find /scripts/lib/dave.js");
      }
      System.out.println("*****FOUND /scripts/lib/dave.js **********8");
   }
}
